var select = document.getElementById('cmbNum');
// Llena el combobox con números del 1 al 10
for (var i = 1; i <= 10; i++) {
    var option = document.createElement('option');
    option.text = i;
    option.value = i;
    select.add(option);
}
// Función para mostrar la tabla de multiplicación con imágenes
btnTabla.addEventListener('click', function() {
    // Obtén el número seleccionado del combobox
    var numero = parseInt(select.value);

    // Limpia el contenido anterior de la tabla de resultado
    tablaResultado.innerHTML = '';

    // Crea las filas de la tabla de multiplicación con imágenes
    for (var i = 1; i <= 10; i++) {
        var fila = document.createElement('div');
        fila.className = 'fila';
        
        // Crea y agrega las imágenes y números a la fila
        var imgNumero1 = document.createElement('img');
        imgNumero1.src = '/img/' + numero + '.png'; // Reemplaza 'images/' con la ruta correcta a tus imágenes
        fila.appendChild(imgNumero1);
        
        var imgMultiplicacion = document.createElement('img');
        imgMultiplicacion.src = '/img/x.png'; // Reemplaza 'images/' con la ruta correcta a tus imágenes
        fila.appendChild(imgMultiplicacion);
        
        var imgNumero2 = document.createElement('img');
        imgNumero2.src = '/img/' + i + '.png'; // Reemplaza 'images/' con la ruta correcta a tus imágenes
        fila.appendChild(imgNumero2);
        
        var imgIgual = document.createElement('img');
        imgIgual.src = '/img/=.png'; // Reemplaza 'images/' con la ruta correcta a tus imágenes
        fila.appendChild(imgIgual);

        // Calcula el resultado de la multiplicación
        var resultadoMultiplicacion = numero * i;
        
        // Divide el resultado en dígitos individuales
        var digitos = resultadoMultiplicacion.toString().split('');
        
        // Crea elementos de imagen para cada dígito y agrégales las imágenes correspondientes
        digitos.forEach(function(digito) {
            var imgDigito = document.createElement('img');
            imgDigito.src = '/img/' + digito + '.png'; // Reemplaza 'images/' con la ruta correcta a tus imágenes
            fila.appendChild(imgDigito);
        });

        // Agrega la fila a la tabla de resultado
        tablaResultado.appendChild(fila);
    }
});
