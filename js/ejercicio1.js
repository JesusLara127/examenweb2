var select = document.getElementById('cmbNum');
        
// Llena el combobox con números del 1 al 10
for (var i = 1; i <= 10; i++) {
    var option = document.createElement('option');
    option.text = i;
    option.value = i;
    select.add(option);
}
// Obtén el elemento select por su ID
var select = document.getElementById('cmbNum');

// Obtén el elemento donde se mostrará la tabla
var tablaResultado = document.getElementById('tablaResultado');

// Obtén el botón por su ID
var btnTabla = document.getElementById('btnTabla');

// Función para mostrar la tabla de multiplicación con imágenes
btnTabla.addEventListener('click', function() {
    // Obtén el número seleccionado del combobox
    var numero = parseInt(select.value);

    // Limpia el contenido anterior de la tabla de resultado
    tablaResultado.innerHTML = '';

    // Crea las filas de la tabla de multiplicación con imágenes
    for (var i = 1; i <= 10; i++) {
        var fila = document.createElement('div');
        fila.className = 'fila';
        
        // Crea y agrega las imágenes y números a la fila
        var imgNumero1 = document.createElement('img');
        imgNumero1.src = '/img/' + numero + '.png'; // Reemplaza 'images/' con la ruta correcta a tus imágenes
        fila.appendChild(imgNumero1);
        
        var imgMultiplicacion = document.createElement('img');
        imgMultiplicacion.src = '/img/x.png'; // Reemplaza 'images/' con la ruta correcta a tus imágenes
        fila.appendChild(imgMultiplicacion);
        
        var imgNumero2 = document.createElement('img');
        imgNumero2.src = '/img/' + i + '.png'; // Reemplaza 'images/' con la ruta correcta a tus imágenes
        fila.appendChild(imgNumero2);
        
        var imgIgual = document.createElement('img');
        imgIgual.src = '/img/=.png'; // Reemplaza 'images/' con la ruta correcta a tus imágenes
        fila.appendChild(imgIgual);

        // Crea el elemento span para mostrar el resultado de la multiplicación
        var resultado = document.createElement('span');
        resultado.textContent = numero * i;
        fila.appendChild(resultado);

        // Agrega la fila a la tabla de resultado
        tablaResultado.appendChild(fila);
    }
});
